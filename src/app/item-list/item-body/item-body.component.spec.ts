import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBodyComponent } from './item-body.component';

describe('ItemBodyComponent', () => {
  let component: ItemBodyComponent;
  let fixture: ComponentFixture<ItemBodyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ItemBodyComponent]
    });
    fixture = TestBed.createComponent(ItemBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
