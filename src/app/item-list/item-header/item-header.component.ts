import { Component, Input, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from 'src/app/item';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'his-item-header',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './item-header.component.html',
  styleUrls: ['./item-header.component.scss']
})
export class ItemHeaderComponent {


  isEdit = false;
  origItem!: Item;
  @Input() item = signal([] as Item[]);
  isDialogOpen = false;
  date!: Date;

}
