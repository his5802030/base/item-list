import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item';
import { ItemHeaderComponent } from './item-header/item-header.component';
import { ItemBodyComponent } from './item-body/item-body.component';

@Component({
  selector: 'his-item-list',
  standalone: true,
  imports: [CommonModule ,ItemHeaderComponent ,ItemBodyComponent],
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent {
  @Input() item!: Item;
}
